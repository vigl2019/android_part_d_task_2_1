package com.example.android_part_d_task_2_1;

/*

2.1* Добавить возможность добавлять контакты в список на первом экране.
(метод у адаптера notifyDataSetChanged())

*/

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_d_task_2_1.model.Person;
import com.example.android_part_d_task_2_1.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_EMAIL = "extra_email";
    public static final String EXTRA_ADDRESS = "extra_address";
    public static final String EXTRA_PHONE_NUMBER = "extra_phoneNumber";
    public static final String EXTRA_AVATAR = "extra_avatar";

    @BindView(R.id.am_persons_list)
    RecyclerView recyclerView;

    @BindView(R.id.am_add_name)
    EditText amAddName;

    @BindView(R.id.am_add_email)
    EditText amAddEmail;

    @BindView(R.id.am_add_address)
    EditText amAddAddress;

    @BindView(R.id.am_add_phone_number)
    EditText amAddPhoneNumber;

    @BindView(R.id.am_add_person_info)
    Button amAddPersonInfo;

    List<Person> persons;

    PersonAdapter personAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        persons = Helper.generateListPerson();

        //     RecyclerView recyclerView = findViewById(R.id.am_persons_list);
        personAdapter = new PersonAdapter(this, persons);
        recyclerView.setAdapter(personAdapter);

        personAdapter.setListener(new PersonAdapter.OnItemClickListener() {
            @Override
            public void onClick(Person person) {
                sendIntent(person);
            }
        });

        amAddPersonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSinglePerson();
            }
        });
    }

    //================================================================================//

    private void sendIntent(Person person) {

        Intent intent = new Intent(this, PersonInfoActivity.class);

        intent.putExtra(EXTRA_NAME, person.getName());
        intent.putExtra(EXTRA_EMAIL, person.getEmail());
        intent.putExtra(EXTRA_ADDRESS, person.getAddress());
        intent.putExtra(EXTRA_PHONE_NUMBER, person.getPhoneNumber());

        int avatarImageId = this.getResources().getIdentifier(person.getAvatar(), "drawable", this.getPackageName());
        intent.putExtra(EXTRA_AVATAR, avatarImageId);

        startActivity(intent);
    }

    //================================================================================//

    private void addSinglePerson() {

        Person person = new Person();

        person.setAvatar("a20");
        person.setName(amAddName.getText().toString().trim());
        person.setEmail(amAddEmail.getText().toString().trim());
        person.setAddress(amAddAddress.getText().toString().trim());
        person.setPhoneNumber(amAddPhoneNumber.getText().toString().trim());

        persons.add(0, person);

/*
        persons.add(person);
        int index = persons.size();
*/

        personAdapter.notifyItemInserted(0);

        //==================================================//

        amAddName.setText("");
        amAddEmail.setText("");
        amAddAddress.setText("");
        amAddPhoneNumber.setText("");

        Toast.makeText(this, "Person has been added successfully!", Toast.LENGTH_LONG).show();
    }
}

