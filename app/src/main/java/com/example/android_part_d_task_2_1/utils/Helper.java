package com.example.android_part_d_task_2_1.utils;

import com.example.android_part_d_task_2_1.model.Person;

import java.util.ArrayList;
import java.util.List;

public class Helper {

    public static List<Person> generateListPerson() {

        List<Person> persons = new ArrayList<>();

        for (int i = 1; i <= 20; i++)
            persons.add(generatePerson(i));

        return persons;
    }

    public static Person generatePerson(int i) {

        Person person = new Person();

        person.setName("Name_" + i);
        person.setEmail("person_email_" + i + "@person.com");
        person.setAddress("address_" + i);
        person.setPhoneNumber("+1(123)456789" + i);
        person.setAvatar("a" + i);
/*
            person = new Person("Name_" + i, "person_email_" + i + "@person.com", "address_" + i,
                    "+1(123)456789" + i, "a" + i);
*/

        return person;
    }
}

